export const wwwDir = '/Users/Christian/shortbox-sandbox/shortbox-react/public';
export const coverDir = 'covers';
export const db = 'shortbox_old';
export const dbUser = 'root';
export const dbPassword = '';

export const fixOnStartup = false;
export const migrateOnStartup = false;
export const afterFirstMigration = true;
export const migration_db = 'shortbox';
export const migration_dbUser = 'root';
export const migration_dbPassword = '';